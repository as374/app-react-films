import * as React from 'react';
import axios from 'axios';
import { useState, useEffect } from 'react'
import Afficher from './Afficher';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import Divider from '@mui/material/Divider';
import DetailFilms from './DetailFilms';


const baseURL = "https://api.themoviedb.org/3/movie/now_playing?api_key=e2541cc19163b21af86c2f9117a0363c&language=fr-FR&page=1";


export default function ListeFilms(props) {

    const [affichageFilms, setFilms] = useState([]);
    const [titreFilm, setTitreFilm] = useState('');
    const [idFilm, setDataId] = useState('');
    const [noteFilm, setNoteFilm] = useState('');
    const [imgFilm, setImgFilm] = useState('');
    const [synopsysFilm, setSynopsysFilm] = useState('');

    const setIdFilmIdOn = (id) => {
        setDataId(id);
    }
    const setTitreFilmOn = (title) => {
        setTitreFilm(title);
    }
    const setNoteFilmOn = (note) => {
        setNoteFilm(note);
    }
    const setImgFilmOn = (img) => {
        setImgFilm(img);
    }
    const setSynopsysFilmOn = (synopsys) => {
        setSynopsysFilm(synopsys);
    }

    useEffect(() => {
        axios.get(baseURL).then((response) => {
            setFilms(response.data.results);
        })
            .catch((error) => console.log(error));
    }, [])

    const listeFiltre = affichageFilms.filter((filtre) => {
        if (props.input === '') {
            return filtre;
        } else {
            return filtre.title.toLowerCase().includes(props.input)
        }
    })

    return (

        <div className='listeFilms'>
            <List sx={{
                width: '100%',
                maxWidth: 350,
                minWidth: 350,
                bgcolor: 'background.paper',
                position: 'relative',
                overflow: 'auto',
                maxHeight: '91vh',
                '& ul': { padding: 0 },
            }} subheader={<li />} >
                {[0].map((sectionId) => (
                    <li key={`section-${sectionId}`}>
                        <ul>

                            {listeFiltre.map((film) => (
                                <nav aria-label="secondary mailbox folders">
                                    <List >
                                        <ListItem disablePadding>
                                            <ListItemButton onClick={() => { setTitreFilmOn(film.title); setIdFilmIdOn(film.id); setNoteFilmOn(film.vote_average); setImgFilmOn(film.backdrop_path); setSynopsysFilmOn(film.overview) }}>
                                                <ListItemText key={film.id}>{film.title}</ListItemText>
                                            </ListItemButton>
                                        </ListItem>
                                        <Divider color="#c0965e" />
                                    </List>
                                </nav>
                            ))}
                        </ul>
                    </li>
                ))}
            </List>
            <div className='box-detailFilms'>
                {titreFilm ? <DetailFilms senDFilmTitre={titreFilm} senDFilmId={idFilm} senDFilmNote={noteFilm} senDFilmImg={imgFilm} senDFilmSynopsys={synopsysFilm} /> : <Afficher />}
            </div>
        </div>
    );

}