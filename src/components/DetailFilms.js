import './../App.css';
import * as React from 'react';
import Box from '@mui/material/Box';
import Rating from '@mui/material/Rating';
import Typography from '@mui/material/Typography';

function DetailFilms(props) {

	return (

		<div className='main-detail' key={props.senDFilmId}>

			<div>
				<img src={`https://image.tmdb.org/t/p/original${props.senDFilmImg}`} className='affiche-detail' />
			</div>

			<Box className='description-detail' sx={{ '& > legend': { mt: 2 }, }} >
				<Typography variant="h3" component="legend">{props.senDFilmTitre}</Typography>
				<Rating name="read-only" value={props.senDFilmNote} max={10} precision={0.5} readOnly />
				<Typography component="legend">{props.senDFilmSynopsys}</Typography>
			</Box>

		</div>
	)
}

export default DetailFilms