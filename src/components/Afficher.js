import './../App.css';
import * as React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import logo from './../resources/logoFilm.png';

function Afficher() {

    return (

        <div className='main-detail' >

            <div>
                <img src={logo} className='affiche-detail' />
            </div>

            <Box className='description-detail' sx={{ '& > legend': { mt: 2 }, }} >
                <Typography variant="h3" component="legend">Choisissez un film</Typography>
                <Typography component="legend">Un site en React</Typography>
            </Box>

        </div>
    )
}

export default Afficher