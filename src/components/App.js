import './../App.css';
import ListeFilms from './ListeFilms';
import { React, useState } from "react";
import TextField from "@mui/material/TextField";

function App() {

    const [inputText, setInputText] = useState("");

    let inputHandler = (e) => {
        var lowerCase = e.target.value.toLowerCase();
        setInputText(lowerCase);
    };

    return (
        <div className="liste-films">
            <div className="recherche">
                <TextField
                    id="outlined-basic"
                    onChange={inputHandler}
                    variant="outlined"
                    fullWidth
                    label="Search" />
            </div>
            <ListeFilms input={inputText} />
        </div>

    )

}

export default App;
