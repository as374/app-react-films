## Prérequis
Afin de pouvoir exécuter l'application sur votre poste, vous devez d'aborder installer les dépendances suivantes :
  * npm
  * CRA
 
### Installation
#### NPM
  1. npm install
  2. npm install @mui/material @emotion/react @emotion/styled
  3. npm install @mui/material @mui/styled-engine-sc styled-components
  4. npm install axios
  5. npm start
 
